﻿using System;
using Gtk;
using Cabinet;
using System.Collections.Generic;
using System.IO;

public partial class MainWindow: Gtk.Window, StatusAble
{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
		this.clearStatus ();

		//filechooserbutton1.SetUri ("file:///C:\\Users\\Nicolas Torchio\\cabinet\\Cabinet\\Files/N201701242.txt");
		//filechooserbutton2.SetUri ("file:///C:\\Users\\Nicolas Torchio\\cabinet\\Cabinet\\Files/SAT_response_Thu_Feb__2_01-00-02_2017.txt");
		//filechooserbutton6.SetUri ("file:///C:\\\\Users\\\\Nicolas Torchio\\\\cabinet\\\\Cabinet\\\\Files/00391944.prt");
		//entry1.Text = "uruGuay";
		//filechooserbutton3.SetUri("file:///C:\\\\Users\\\\Nicolas Torchio\\\\cabinet\\\\Cabinet\\\\Files/00391944.prt");
		//filechooserbutton4.SetUri ("file:///C:\\Users\\Nicolas Torchio\\cabinet\\Cabinet\\Files/Enero.TXT");
		//filechooserbutton4.SetUri("file:///C:\\Users\\Nicolas Torchio\\cabinet\\Cabinet\\Files/Febrero.TXT");
		//filechooserbutton5.SetUri("file:///C:\\\\Users\\\\Nicolas Torchio\\\\cabinet\\\\Cabinet\\\\Files/00391944.prt");

		OnButton1Clicked (null,null);
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	public void setStatus(string status){
		textview1.Buffer.Text += status + "\r";
		label4.Text = status;
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
	}

	public void clearStatus(){
		textview1.Buffer.Text = "";
		label4.Text = "Listo";
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
		while (Gtk.Application.EventsPending ())
			Gtk.Application.RunIteration ();
	}

	protected void OnButton1Clicked (object sender, EventArgs e)
	{
		clearStatus ();
		HBESATUtil hsu = new HBESATUtil ();
		SAPUtil sau = new SAPUtil ();
		SisConUtil scu = new SisConUtil ();
		SWIFTUtil swu = new SWIFTUtil (); 

		try{
			if(filechooserbutton1.Filename != null && filechooserbutton2.Filename != null &&  !filechooserbutton1.Filename.Equals("") && !filechooserbutton2.Filename.Equals("")){
				HBESAT sat = hsu.readHBESAT(filechooserbutton1.Filename,filechooserbutton2.Filename,this);
				scu.saveHBESAT (sat,"Salida_Sat.txt",this);
			}
		} catch (Exception ex){
			setStatus (ex.ToString());
		}
		try{
			if(filechooserbutton4.Filename != null && !filechooserbutton4.Filename.Equals("")){
				SAP sap = sau.readSAP(filechooserbutton4.Filename,this);
				scu.saveSAP (sap,"Salida_Sap.txt",this);
			}
		} catch (Exception ex){
			setStatus (ex.ToString());
		}

		try{
			if(filechooserbutton3.Filename != null && !filechooserbutton3.Filename.Equals("")){
				SWIFT swift = swu.readSWIFT(filechooserbutton3.Filename,this);
				scu.saveSWIFT (swift,"Salida_Swift.txt",entry1.Text,this);
			}
		} catch (Exception ex){
			setStatus (ex.ToString());
		}

		try
		{
			if (filechooserbutton6.Filename != null && !filechooserbutton6.Filename.Equals(""))
			{
				string[] myFiles = Directory.GetFiles(System.IO.Path.GetDirectoryName(filechooserbutton6.Filename), "*.*", SearchOption.AllDirectories);
				File.Delete("Salida_Swift.txt");
				List<SWIFT> swifts = new List<SWIFT>();
				foreach (string fila in myFiles) {
					Console.WriteLine(fila);
					if (fila.ToLower().EndsWith("prt")) { 
						SWIFT swift = swu.readSWIFT(fila, this);
						swifts.Add(swift);
					}
				}
				scu.saveSWIFT(swifts, "Salida_Swift.txt",entry1.Text, this);
			}
		}
		catch (Exception ex)
		{
			setStatus(ex.ToString());
		}

		try{
			if(filechooserbutton5.Filename != null && !filechooserbutton5.Filename.Equals("")){
				scu.readAndSaveUTF8 (filechooserbutton5.Filename,"Salida_UTF8.txt",this);
			}
		} catch (Exception ex){
			setStatus (ex.ToString());
		}

		filechooserbutton1.SetUri (null);
		filechooserbutton2.SetUri (null);
		filechooserbutton3.SetUri (null);
		filechooserbutton4.SetUri (null);
		filechooserbutton5.SetUri (null);
		filechooserbutton6.SetUri (null);

		setStatus ("Listo");
	}
}
