﻿using System;

namespace Cabinet
{
	public interface StatusAble
	{
		void setStatus(string status);
		void clearStatus();
	}
}

