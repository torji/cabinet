﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Cabinet
{
	public class HBESATUtil
	{
		public HBESAT readHBESAT(string path1, string path2, StatusAble statusable){
			statusable.setStatus ("Leyendo archivo " + path1);
			string[] lines = File.ReadAllLines(path1);
			statusable.setStatus ("Leidas " + lines.Length + " lineas");
			statusable.setStatus ("Leyendo archivo " + path2);
			string[] lineas = File.ReadAllLines(path2);
			statusable.setStatus ("Leidas " + lineas.Length + " lineas");

			HBESAT resultado = new HBESAT ();
			resultado.header.cuentaDebito = lines [0].Substring (26, 10);
			resultado.header.moneda = lines [0].Substring (47, 4);

			statusable.setStatus ("Mergeando files");
			for (int i = 1; i < lines.Length - 1; i++) {
				bool sondeeste = false;
				foreach (string linea in lineas) {
					if (linea.Substring (0, 2).Equals ("N1")) {
						if (lines [i].Substring (75, 9).Equals (linea.Substring (22, 9))) {
							sondeeste = true;
						} else {
							sondeeste = false;
						}
					}
					if (sondeeste && linea.Substring (0, 2).Equals ("QN")) {
						HBESATDetalle detalle = new HBESATDetalle ();
						//detalle.tipoTransferencia = ;
						detalle.idTransferencia = lines[i].Substring (75, 9);
						detalle.importe = lines [i].Substring (45, 17);
						detalle.fecha = lines [i].Substring (37, 8);
						detalle.cbu = lines [i].Substring (4, 22);
						detalle.paso = linea.Substring (10, 6);
						detalle.subpaso = linea.Substring (76, 3);
						resultado.detalle.Add (detalle);
					}
				}
			}
			statusable.setStatus ("Procesadas " + resultado.detalle.Count + " lineas");
			return resultado;
		}
	}

	public class HBESAT
	{
		public HBESATHeader header = new HBESATHeader();
		public List<HBESATDetalle> detalle = new List<HBESATDetalle>();
	}

	public class HBESATHeader
	{
		public string cuentaDebito;
		public string moneda;
	}

	public class HBESATDetalle
	{
		public string tipoTransferencia;
		public string idTransferencia;
		public string cbu;
		public string importe;
		public string fecha;
		public string paso;
		public string subpaso;
	}
}

