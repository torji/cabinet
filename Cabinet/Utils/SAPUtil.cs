﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Cabinet
{
	public class SAPUtil
	{
		public SAP readSAP(string path1, StatusAble statusable){
			statusable.setStatus ("Leyendo archivo " + path1);

			string[] lines = File.ReadAllLines(path1);

			SAP sap = new SAP ();
			SAPHeader headInUse = null;
			int c = 0;
			int d = 0;
			foreach(string line in lines){
				if (line.Length != 132 && line.Length != 128 && line.Length != 131 && line.Length != 98  && line.Length != 92 && line.Length != 76 && line.Length > 70) {
					SAPHeader head = new SAPHeader ();
					head.FeCPU = line.Substring (0, 6).Trim();
					head.Ndoc = line.Substring (7, 10).Trim();
					head.FeCont = line.Substring (18, 6).Trim();
					head.FeDoc = line.Substring (25, 6).Trim();
					head.CD = line.Substring (32, 2).Trim();
					head.NReferencia = line.Substring (35, 16).Trim();
					head.TextoCabeceraDocumento = line.Substring (51, 20).Trim();
					headInUse = head;
					sap.headers.Add (head);
					c++;
				}
				if (line.Length == 128) {
					SAPDetail detail = new SAPDetail ();
					detail.Denom = line.Substring (0, 5).Trim();
					detail.Cuenta = line.Substring (5, 14).Trim();
					detail.Ap = line.Substring (21, 4).Trim();
					detail.C = line.Substring (25, 2).Trim();
					detail.NCta = line.Substring (27, 8).Trim();
					detail.Div = line.Substring (35, 4).Trim();
					detail.Asignación = line.Substring (39, 20).Trim();
					detail.CT = line.Substring (59, 2).Trim();
					detail.E = line.Substring (61, 1).Trim();
					detail.LMayorIV = line.Substring (62, 10).Trim();
					detail.ImporteMT = line.Substring (72, 19).Trim();
					detail.Mon = line.Substring (92, 3).Trim();
					detail.ImpteDebeML = line.Substring (96, 17).Trim();
					detail.ImpteHaberML = line.Substring (111, 17).Trim();
					headInUse.detalles.Add (detail);
					d++;
				}

			}
			statusable.setStatus ("Leidas " + c + " cabeceras y " + d + " detalles");
			return sap;
		}
	}

	public class SAP{
		public List<SAPHeader> headers = new List<SAPHeader>();
	}

	public class SAPHeader{
		public string FeCPU;
		public string Ndoc;
		public string FeCont;
		public string FeDoc;
		public string CD;
		public string NReferencia;
		public string TextoCabeceraDocumento;
		public List<SAPDetail> detalles = new List<SAPDetail>();
	}

	public class SAPDetail{
		public string Denom;
		public string Cuenta;
		public string Ap;
		public string C;
		public string NCta;
		public string Div;
		public string Asignación;
		public string CT ;
		public string E;
		public string LMayorIV;
		public string ImporteMT;
		public string Mon;
		public string ImpteDebeML;
		public string ImpteHaberML;
	}
}

