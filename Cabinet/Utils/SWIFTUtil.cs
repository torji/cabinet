﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace Cabinet
{
	public class SWIFTUtil
	{
		public SWIFT readSWIFT(string path1, StatusAble statusable){
			SWIFT fila = new SWIFT();
			statusable.setStatus ("Leyendo archivo " + path1);
			string lines = File.ReadAllText(path1);
			fila.fecha = lines.Substring(1,13).Trim();
			int m = 0;
			int d = 0;
			int d61 = 0;
			string[] mensajes = Regex.Split(lines,"---------------------  Instance Type and Transmission --------------");
			for (int i = 1; i < mensajes.Length; i++) {
				m++;
				string mensaje = mensajes[i];
				string[] parts = Regex.Split(mensaje,"\t--------------------------- ");
				parts [2] = parts [2].Replace ("Message Text ---------------------------","");
				SwiftMessage mens = new SwiftMessage();
				SwiftMessageText messageInCurso = null;
				SwiftMessageText messageInCurso61 = null;
				bool es61 = false;

				foreach (string linea in parts[2].Split('\n')) {
					if (linea.Length > 10 && linea.Substring (10, 1).Equals (":")) {
						if (!linea.Substring (1, 9).Trim ().Equals ("61")) {
							SwiftMessageText message = new SwiftMessageText ();
							message.movimiento = linea.Substring (1, 9).Trim ();
							message.descripcion = linea.Substring (11).Trim ();
							mens.texts.Add (message);
							d++;
							messageInCurso = message;
							es61 = false;
							if (messageInCurso61 != null) {
								message.Value = messageInCurso61.Value;
								message.Entr = messageInCurso61.Entr;
								message.F = messageInCurso61.F;
								message.Code = messageInCurso61.Code;
								message.Reference = messageInCurso61.Reference;
								message.Amount = messageInCurso61.Amount;
								message.Ma = messageInCurso61.Ma;
								messageInCurso61 = null;
							}
						} else {
							d++;
							d61++;
							es61 = true;
						}
					} else if (messageInCurso != null) {
						if (es61 && !linea.Trim ().Equals ("Value  Entr F Code Reference                      Amount        Ma")) {
							string lin = linea;
							lin += "                                                                               ";
							if (messageInCurso61 == null)
							{
								messageInCurso61 = new SwiftMessageText();
							}
							messageInCurso61.Value += linea.Substring (0,20).Trim();
							messageInCurso61.Entr += linea.Substring (20,4).Trim();
							messageInCurso61.F += linea.Substring (24,1).Trim();
							messageInCurso61.Code += linea.Substring (26,4).Trim();
							messageInCurso61.Reference += lin.Substring (30, 20).Trim () + " ";
							messageInCurso61.Amount += lin.Substring (51, 20).Trim ().Replace("#","");
							messageInCurso61.Ma += lin.Substring (75, 2).Trim ();
						}
						messageInCurso.texto += linea.Trim () + "\n";
					}
				}

				fila.messages.Add(mens);
			}
			statusable.setStatus ("Leidos " + m + " mensajes con " + d + " movimientos de los cuales " + d61 + " son detalles");
			return fila;
		}
	}

	public class SWIFT{
		public string fecha;
		public List<SwiftMessage> messages = new List<SwiftMessage>();
	}

	public class SwiftMessage
	{
		public List<SwiftMessageText> texts = new List<SwiftMessageText>();
	}

	public class SwiftMessageText
	{
		public string movimiento;
		public string descripcion;
		public string texto = "";
		public string Value = "";
		public string Entr = "";
		public string F = "";
		public string Code = "";
		public string Reference = "";
		public string Amount = "";
		public string Ma = "";
	}
}

