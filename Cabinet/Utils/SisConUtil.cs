﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Cabinet
{
	public class SisConUtil
	{
		public void saveSAP(SAP sap, string path, StatusAble statusable){
			statusable.setStatus ("Guardando archivo: " + path);
			int l = 0;
			using(StreamWriter writetext = new StreamWriter(path))
			{
				foreach (SAPHeader head in sap.headers) {
					foreach (SAPDetail detail in head.detalles) {
						writetext.Write (head.FeCPU);
						writetext.Write (";");
						writetext.Write (head.Ndoc);
						writetext.Write (";");
						writetext.Write (head.FeCont);
						writetext.Write (";");
						writetext.Write (head.FeDoc);
						writetext.Write (";");
						writetext.Write (head.CD);
						writetext.Write (";");
						writetext.Write (head.NReferencia);
						writetext.Write (";");
						writetext.Write (head.TextoCabeceraDocumento);
						writetext.Write (";");
						writetext.Write (detail.Denom);
						writetext.Write (";");
						writetext.Write (detail.Cuenta);
						writetext.Write (";");
						writetext.Write (detail.Ap);
						writetext.Write (";");
						writetext.Write (detail.C);
						writetext.Write (";");
						writetext.Write (detail.NCta);
						writetext.Write (";");
						writetext.Write (detail.Div);
						writetext.Write (";");
						writetext.Write (detail.Asignación);
						writetext.Write (";");
						writetext.Write (detail.CT);
						writetext.Write (";");
						writetext.Write (detail.E);
						writetext.Write (";");
						writetext.Write (detail.LMayorIV);
						writetext.Write (";");
						writetext.Write (detail.ImporteMT);
						writetext.Write (";");
						writetext.Write (detail.Mon);
						writetext.Write (";");
						writetext.Write (detail.ImpteDebeML);
						writetext.Write (";");
						writetext.Write (detail.ImpteHaberML);
						writetext.WriteLine(";");
						l++;
					}
				} 
			}
			statusable.setStatus ("Guardadas " + l + " lineas");
		}

		public void saveSWIFT(SWIFT swift, string path, string busqueda, StatusAble statusable)
		{
			List<SWIFT> lista = new List<SWIFT>();
			lista.Add(swift);
			saveSWIFT(lista, path, busqueda, statusable);
		}
			
		public void saveSWIFT(List<SWIFT> swifts, string path, string busqueda, StatusAble statusable){
			statusable.setStatus ("Guardando archivo: " + path);
			int l = 0;
			using(StreamWriter writetext = new StreamWriter(path))
			{
				foreach (SWIFT swift in swifts)
				{
					foreach (SwiftMessage mensage in swift.messages)
					{
						foreach (SwiftMessageText line in mensage.texts)
						{
							writetext.Write(swift.fecha);
							writetext.Write(";");
							writetext.Write(line.movimiento);
							writetext.Write(";");
							writetext.Write(line.descripcion);


							writetext.Write(";");
							writetext.Write(line.Value);
							writetext.Write(";");
							writetext.Write(line.Entr);
							writetext.Write(";");
							writetext.Write(line.F);
							writetext.Write(";");
							writetext.Write(line.Code);
							writetext.Write(";");
							writetext.Write(line.Reference);
							writetext.Write(";");
							writetext.Write(line.Amount);
							writetext.Write(";");
							writetext.Write(line.Ma);

							writetext.Write(";");
							writetext.Write(line.texto.ToLower().Contains(busqueda.ToLower()));

							writetext.Write(";");
							writetext.Write(line.texto.Replace("\n", "").Trim());

							writetext.WriteLine(";");
							l++;
					}
				}
				}
			}
			statusable.setStatus ("Guardadas " + l + " lineas");
		}

		public void saveHBESAT(HBESAT sat, string path, StatusAble statusable){
			statusable.setStatus ("Guardando archivo: " + path);
			int a = 0;
			using(StreamWriter writetext = new StreamWriter(path))
			{
				foreach (HBESATDetalle detalle in sat.detalle) {
					writetext.Write (sat.header.cuentaDebito);
					writetext.Write (";");
					writetext.Write (sat.header.moneda);
					writetext.Write (";");
					writetext.Write (detalle.fecha);
					writetext.Write (";");
					writetext.Write (detalle.idTransferencia);
					writetext.Write (";");
					writetext.Write (detalle.cbu);
					writetext.Write (";");
					writetext.Write (detalle.importe);
					writetext.Write (";");
					writetext.Write (detalle.paso);
					writetext.Write(";");
					writetext.Write (detalle.subpaso);
					writetext.WriteLine(";");
					a++;
				}
			}
			statusable.setStatus ("Guardadas " + a + " lineas");
		}

		public void readAndSaveUTF8(string path, string salida, StatusAble statusable){
			statusable.setStatus ("Leyendo archivo " + path);
			Encoding encoding = Encoding.Default;
			String original = String.Empty;

			using (StreamReader sr = new StreamReader(path, Encoding.Default))
			{
				original = sr.ReadToEnd();
				encoding = sr.CurrentEncoding;
				sr.Close();
			}

			if (encoding != Encoding.UTF8) {
				statusable.setStatus ("Convirtiendo archivo");
				byte[] encBytes = encoding.GetBytes (original);
				byte[] utf8Bytes = Encoding.Convert (encoding, Encoding.UTF8, encBytes);
				original = Encoding.UTF8.GetString (utf8Bytes);
			} else {
				statusable.setStatus ("Archivo ya es utf8");
			}

			using(StreamWriter writetext = new StreamWriter(salida))
			{
				writetext.Write (original);
			}
			statusable.setStatus ("Archivo " + salida + " guardado");
		}
	}
}

